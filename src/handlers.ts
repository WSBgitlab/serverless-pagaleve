import { APIGatewayProxyEvent, APIGatewayProxyResult } from "aws-lambda";
import { v4 } from "uuid";

// sdk
import AWS from "aws-sdk";

// customer document
const document = new AWS.DynamoDB.DocumentClient();

// Table name
const tableName = "CustomersTable";

// Exception
import { HttpErros } from "./exceptions/customerExceptions";

// Find customer ID
const findCustomer = async (id: string) => {
  const actionResponse = await document
    .get({
      TableName: tableName,
      Key: {
        customerID: id,
      },
    })
    .promise();

  if (!actionResponse) {
    throw new HttpErros(400, { error: "Ops! error find customer with id!" });
  }

  return actionResponse.Item;
};

// SEARCH text free

// Wrapper handle error
const handleErros = (error: unknown) => {
  if (error instanceof HttpErros) {
    return {
      statusCode: error.statusCode,
      body: error.message,
    };
  }

  throw error;
};

export const createCustomer = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  // parsing body
  const body = JSON.parse(event.body as string);
  const customer = {
    ...body,
    customerID: v4(),
  };

  await document
    .put({
      TableName: tableName,
      Item: customer,
    })
    .promise();

  return {
    statusCode: 200,
    body: JSON.stringify(customer),
  };
};

export const getCustomer = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  try {
    const customer = await findCustomer(event.pathParameters?.id as string);

    return {
      statusCode: 200,
      body: JSON.stringify(customer),
    };
  } catch (error) {
    return handleErros(error);
  }
};

export const listCustomer = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  const listCustomer = await document
    .scan({
      TableName: tableName,
    })
    .promise();

  return {
    statusCode: 200,
    body: JSON.stringify(listCustomer.Items),
  };
};

export const updateCustomer = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  // get id customer
  const customerID = event.pathParameters?.id as string;

  await findCustomer(customerID);

  const body = JSON.parse(event.body as string);
  try {
    const customer = {
      ...body,
      customerID: customerID,
    };

    await document
      .put({
        TableName: tableName,
        Item: customer,
      })
      .promise();

    return {
      statusCode: 200,
      body: JSON.stringify(customer),
    };
  } catch (error) {
    return handleErros(error);
  }
};

export const deleteCustomer = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  try {
    const customerID = event.pathParameters?.id as string;

    await findCustomer(customerID);

    await document
      .delete({
        TableName: tableName,
        Key: {
          customerID: customerID,
        },
      })
      .promise();

    return {
      statusCode: 204,
      body: JSON.stringify({
        message: "Delete Customer successfully!",
      }),
    };
  } catch (error) {
    return handleErros(error);
  }
};

// search user
export const postCustomerSerch = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  try {
    const body = JSON.parse(event.body as string);

    console.log("body", body);

    const searchResponse = await document
      .get({
        TableName: tableName,
        Key: body,
      })
      .promise();

    return {
      statusCode: 200,
      body: JSON.stringify(searchResponse),
    };
  } catch (error) {
    return handleErros(error);
  }
};

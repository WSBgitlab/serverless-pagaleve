# AWS - Serverless Pagaleve ![aws](./docs/emojis_aws.png)

Olá Pessoal, aqui é o repo para documentação de implementação AWS Serverless.

## API Gateway

Criado a partir da stack com serveless.

![API Gateway](./docs/gateway.png)

## Stack

Stack para implementação das funções e API.

![Stack](./docs/stack.png)

## IAM

Criado usuário com grupos para DynamoDB e Lambda permições.

![IAM](./docs/iam.png)

Para configuração do serverless precisamos das credenciais de acesso nesse caso para o usuário lmb-admin, por exemplo:

```bash
export AWS_ACCESS_KEY_ID=
export AWS_SECRET_ACCESS_KEY=

serverless deploy
```

## Deploy to AWS

Apoś os comando de deploy será informado se ocorreu tudo bem com o deploy as seguintes informações serão exibidas:

![Inf Deploy](./docs/deploy_serveless.png)
